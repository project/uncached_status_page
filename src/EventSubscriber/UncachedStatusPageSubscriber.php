<?php

namespace Drupal\uncached_status_page\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Uncached Status Page event subscriber.
 */
class UncachedStatusPageSubscriber implements EventSubscriberInterface {

  /**
   * Routes to not cache.
   *
   * @var string[]
   */
  protected array $routes = [
    'uncached_status_page.status',
  ];

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::RESPONSE => ['onResponse', -100],
    ];
  }

  /**
   * Kernel response callback.
   *
   * @param ResponseEvent $event
   *   Kernel event.
   */
  public function onResponse(ResponseEvent $event): void {
    if (in_array($event->getRequest()->get('_route'), $this->routes, true)) {
      $response = $event->getResponse();
      $response->headers->addCacheControlDirective('no-store', TRUE);
      $response->headers->addCacheControlDirective('must-revalidate', TRUE);
      $response->headers->addCacheControlDirective('no-cache', TRUE);
      $response->headers->addCacheControlDirective('max-age', 0);
      $response->headers->set('Expires', 0);
    }
  }

}
