<?php

namespace Drupal\uncached_status_page\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Uncached Status Page routes.
 */
class UncachedStatusPageController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('Site is Up!'),
    ];

    return $build;
  }

}
