# Uncached Satus Page

This module provides an uncached status page to use with monitoring tools and services. It is basic and could be
extended later to offer configurbale messages and routes but, for now, it is simply one uncached page with a static
route.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/uncached_status_page).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/uncached_status_page).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Uncached Status Page need to be installed with composer.
See [Using Composer with Drupal](https://www.drupal.org/node/2404989)
for more information.


## Configuration

There is no configuration for this module. Enable it and a page will appear at `/uncached_status_page/status`.
Use this url with your monitoring services.


## Maintainers

Current maintainers:
- Aikaterine Tsiboukas - [bletch](https://www.drupal.org/u/bletch)